<?php
namespace Econda\Util;

/**
 * Provides common string related functions. All functions must be static.
 */
class StringUtil
{
	/**
	 * Like html_entity_decode but decodes utf entities &#00; / &#x00; too
	 * @param string $html
	 * @return string
	 */
	public static function htmlDecode($text)
	{
		if(strpos($text, '&') !== false) {
			$text = html_entity_decode($text);
			$text = preg_replace_callback('/&#(\d+);/m', function($p){ return chr($p[1]); }, $text); #decimal notation
			$text = preg_replace_callback('/&#x([a-f0-9]+);/mi', function($p){ return chr('0x'.$p[1]);}, $text); #hex notation
		}
		return $text;
	}
}