<?php

namespace Econda\Util;

/**
 * Base object that provides some standard methods.
 * 
 * @author cluetjen
 */
abstract class BaseObject {

    /**
     * Default constructor. Allows us to set properties using an array as single constructor argument.
     * 
     * @param string $arguments
     * @throws \InvalidArgumentException
     */
    public function __construct($arguments = null) {
        if ($arguments == null) {
            return;
        }

        if (is_array($arguments) == false) {
            throw new \InvalidArgumentException("Standard constructor expects an array of properties.");
        }
        foreach ($arguments as $key => $value) {
            $propertyName = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $key))));
            $this->__set($propertyName, $value);
        }
    }

    /**
     * Handles get operations on undefined properties.
     * Call getter if defined, return property value if defined or throw an exception.
     *  
     * @param string $propertyName
     * @throws \RuntimeException
     */
    public function __get($propertyName) {
        $getterName = "get" . ucfirst($propertyName);
        if (method_exists($this, $getterName)) {
            return $this->$getterName();
        } else if (property_exists($this, $propertyName)) {
            return $this->$propertyName;
        } else {
            throw new \RuntimeException("Property not found: $propertyName in class " . get_class($this));
        }
    }

    /**
     * Handles set operations on undefined properties.
     * Use setter function if defined, set property if defined (as protected property) or
     * throw an exception if there's no setter and no property.
     * 
     * @param string $propertyName
     * @param mixed $value
     * @throws \RuntimeException
     * @return \Econda\Util\BaseObject
     */
    public function __set($propertyName, $value) {
        $setterName = "set" . ucfirst($propertyName);
        if (method_exists($this, $setterName)) {
            $this->$setterName($value);
        } else if (property_exists($this, $propertyName)) {
            $this->$propertyName = $value;
        } else {
            throw new \RuntimeException("Property not found: $propertyName in class " . get_class($this));
        }
    }

    /**
     * Handles calls to getters and setters if there's no real function available
     * 
     * @param string $methodName
     * @param array $arguments
     * @throws \Exception
     * @return \Econda\Util\BaseObject
     */
    public function __call($methodName, $arguments) {
        switch (substr($methodName, 0, 3)) {
            case "get":
                $propertyName = lcfirst(substr($methodName, 3));
                return $this->__get($propertyName);
            case "add":
                $propertyName = lcfirst(substr($methodName, 3)) . "s";
                array_push($this->$propertyName, $arguments[0]);
                return $this;
            case "set":
                $propertyName = lcfirst(substr($methodName, 3));
                $this->__set($propertyName, $arguments[0]);
                return $this;
            default:
                throw new \Exception("Unknown method: $methodName");
        }
    }

    /**
     * Returns all propertes as an array
     * @return array
     */
    public function toArray() {
        $ref = new \ReflectionObject($this);
        $properties = $ref->getProperties(\ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PUBLIC);
        $result = array();
        foreach ($properties as $property) {
            $result[$property->getName()] = $this->__get($property->getName());
        }

        return $result;
    }
}
