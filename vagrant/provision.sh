#!/usr/bin/env bash

apt-get update -yq
apt-get install -yq curl php5-cli git

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer