econda/shared
===============

Shared classes.

### Installing via Composer

The recommended way to install this library is through [Composer](http://getcomposer.org).

### Documentation
For further documentation, please visit our support portal https://support.econda.de
